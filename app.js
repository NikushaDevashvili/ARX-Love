'use strict';

const express = require('express');
const smsOffice = require('./smsoffice/smsoffice.js');
const bodyParser = require('body-parser');
const firebaseAdmin = require('firebase-admin');
var serviceAccount = require('./arx-love-78f98-firebase-adminsdk-mbs12-44c000cd53.json');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'jade');
app.use('/assets', express.static('assets'));

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: "https://arx-love-78f98.firebaseio.com",
  databaseAuthVariableOverride: {
    uid: "my-service-worker"
  }
});

// The app only has access as defined in the Security Rules
var db = firebaseAdmin.database();

app.get('/', function(req, res) {
    res.render("index");
});

app.get('/submit', function(req, res) {
    res.render('submitForm');
});

app.post('/submit', function(req, res) {
    var data = req.body;
    var weloveRef = db.ref("/Company/Arx/welove/");
    // weloveRef.orderByChild("PhoneNumber").equalTo(data["PhoneNumber"]).once("value", function(snapshot){
    //     console.log(snapshot.val());
    //     if (snapshot.val() == null) {
    data["Timestamp"] = firebaseAdmin.database.ServerValue.TIMESTAMP;
    weloveRef.push().set(data);
    res.status(200).send("დამატება დასრულდა წარმატებით.");
    //     } else {
    //         res.status(200).send("ტექსტი მოცემულ ნომერზე უკვე გაგზავნილია.");
    //     }
    // }, function (errorObject){
    //     console.log("The read failed: " + errorObject.code);
    //     res.sendStatus(500);
    //     res.status(500).send("კავშირის დამყარება ვერ მოხერხდა სცადეთ მოგვიანებით.")
    // });
    
});

app.get('/report/:postId', function(req, res) {
    var postId = req.params["postId"];
    var ref = db.ref("/Company/Arx/welove/" + postId);
    ref.once("value", function(snapshot) {
        var data = snapshot.val();
        ref.set({
            Latitude: data.Latitude,
            Longitude: data.Longitude,
            Name: data.Name,
            PhoneNumber: data.PhoneNumber,
            Reported: true,
            SelectedGifId: data.SelectedGifId,
            StreetAddress: data.StreetAddress,
            Text: data.Text,
            Timestamp: data.Timestamp
        });
        res.sendStatus(200);
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
        res.sendStatus(500);
    });
});

app.listen(3000, function() {
    console.log('Listening on port 3000!');
});