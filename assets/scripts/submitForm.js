var submitButton = document.getElementById("submit-button");

var SubmitForm = {
    nameField: document.getElementById("name"),
    name: "",
    textField: document.getElementById("text"),
    text: "",
    numberField: document.getElementById("number"),
    phoneNum: "",
    gifSelectionField: document.getElementById("gif-selector"),
    selectedGifId: 1,
    addressField: document.getElementById("locText"),
    address: "",
    latField: document.getElementById("latitudeText"),
    latitude: "",
    longField: document.getElementById("longitudeText"),
    longitude: "",

    SetAddress: function(newAddress) {
        this.addressField.value = newAddress;
    },

    SetLatitdue: function(newLat) {
        this.latField.value = newLat;
    },

    SetLongitude: function(newLng) {
        this.longField.value = newLng;
    },

    UpdateFieldsFromInputs: function() {
        this.name = this.nameField.value;
        this.text = this.textField.value;
        this.phoneNum = this.numberField.value;
        // this.selectedGifId this is always updated onClick
        this.address = this.addressField.value;
        this.latitude = this.latField.value;
        this.longitude = this.longField.value;
    },

    IsValid: function() {
        if (this.name == "") {
            alert("სახელი არ შეიძლება იყოს ცარიელი");
            return false;
        }

        if (this.text == "") {
            alert("ტექსტი არ შეიძლება იყოს ცარიელი");
            return false;
        }

        if (this.phoneNum == "" || this.phoneNum.length != 9) {
            alert("მობილურის ნომერი არ შეიძლება იყოს ცარიელი ან 9 სიმბოლოზე ნაკლები რაოდენობის");
            return false;
        }

        if (this.address == "") {
            return false;
        }

        if (this.latitude == "") {
            ShowNotification();
            return false;
        }

        if (this.longitude == "") {
            ShowNotification();
            return false;
        }

        return true;
    },

    GetAsObject: function() {
        var data = {
            Name: this.name,
            Text: this.text,
            PhoneNumber: this.phoneNum,
            Type: this.selectedGifId,
            StreetAddress: this.address,
            Latitude: this.latitude,
            Longitude: this.longitude,
            Reported: false
        };

        return data;
    },

    GetAsJsonString: function() {
        return JSON.stringify(this.GetAsObject());
    },

    SelectGif: function(newSelectionElement, id) {
        this.selectedGifId = id;
        this.DeselectChildren();
        newSelectionElement.setAttribute("id", "gif-item-selected");
    },

    DeselectChildren: function() {
        var children = this.gifSelectionField.getElementsByClassName("gif-selectable-item");
        for (var i = 0; i < children.length; i++) {
            children[i].setAttribute("id", "");
        }
    },

    Send: function(jsonDataString) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/submit", true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

        xhr.send(jsonDataString);

        xhr.onloadend = this.ShowEndNotification;

        xhr.onerror = this.ShowErrorNotification;
    },

    ShowErrorNotification: function(eventTarget, ev) {
        console.log(eventTarget);
        if (eventTarget.currentTarget.status == 200) {
            ShowNotification(eventTarget.currentTarget.responseText);
        }
    },

    ShowEndNotification: function(eventTarget, ev) {
        console.log(eventTarget);
        if (eventTarget.currentTarget.status == 200) {
            ShowNotification(eventTarget.currentTarget.responseText);
        }
    },

    TrySend: function() {
        this.UpdateFieldsFromInputs();
        if (this.IsValid())
            this.Send(this.GetAsJsonString());
    }
};

document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function ShowNotification(message) {
    document.getElementById("submit-button").disabled = false;

    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.'); 
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Arx', {
            icon: 'https://firebasestorage.googleapis.com/v0/b/arx-love-78f98.appspot.com/o/ast%2Farxlogo.png?alt=media&token=f265048c-5f64-4cdc-b7b1-27c5196c1f7b',
            body: message,
        });

        notification.onclick = function () {
            window.open("/");      
        }
    }
}
