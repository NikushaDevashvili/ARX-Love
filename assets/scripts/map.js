var lastMarker;

function initMap() {
    var lastMarker = new google.maps.Marker();

    var tbilisi = { lat: 41.7097478, lng: 44.7857329 };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: tbilisi,
        draggableCursor: 'default',
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow({
        content: '<p>Marker Location:' + lastMarker.getPosition() + '</p>'
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            if (lastMarker != null) {
                lastMarker.setMap(null);
            }
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            placeMarker(pos, map);
            geocodePosition(pos);
        }, function() {
            ShowNotification("თქვენი გეოგრაფიული ლოკაციის მოძებნა ვერ მოხერხდა.");
            if (lastMarker != null) { lastMarker.setMap(null); }
            placeMarker(tbilisi, map);
            geocodePosition(tbilisi);
        });
    } else {
        ShowNotification("გეოგრაფიული ლოკაციიზე ქვდომა არ არის ნებადართული.");
        if (lastMarker != null) { lastMarker.setMap(null); }
        placeMarker(tbilisi, map);
        geocodePosition(tbilisi);
    }

    google.maps.event.addListener(map, 'click', function(e) {
        lastMarker.setMap(null);
        placeMarker({lat: e.latLng.lat(), lng: e.latLng.lng()}, map);
    });

    function placeMarker(position, map) {
        lastMarker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: position,
            animation: google.maps.Animation.DROP
        });

        map.panTo(position);

        if (map.zoom == 12) {
            map.setZoom(17);
        }

        geocodePosition(position);

        google.maps.event.addListener(lastMarker, 'click', function(event) {
            infowindow.open(map, lastMarker);
        });

        google.maps.event.addListener(lastMarker, "dragend", function(event) {
            var pos = {
                lat: event.latLng.lat(),
                lng: event.latLng.lng()
            };
            geocodePosition(pos);
        });
    }
}

function geocodePosition(pos) {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({ latLng: pos, "region": "GE" }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            SubmitForm.SetAddress(results[0].formatted_address);
            SubmitForm.SetLatitdue(pos.lat);
            SubmitForm.SetLongitude(pos.lng);
        }
    });
}